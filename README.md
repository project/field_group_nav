# Field group navigation

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

This module generates a pseudo navigation field.

## Requirements

This module requires the [Field Group module](https://drupal.org/project/field_group)

## Installation

Install as usual.

## Configuration

1. To create a field group link, edit the display of any entity (by clicking
   "Manage Display").
2. Add a new group and select the "Nav item" format.
3. Move some fields under the new created field group.
4. Enable "Field group navigation"

## Maintainers

- Adrian Lorenc - [@alorenc](https://www.drupal.org/u/alorenc)
- Colin McCrory - [@kolin](https://www.drupal.org/u/kolin)
