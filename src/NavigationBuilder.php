<?php

declare(strict_types=1);

namespace Drupal\field_group_nav;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Template\Attribute;

/**
 * Implements the navigation service.
 */
class NavigationBuilder implements NavigationBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(array $build, EntityInterface $entity, EntityDisplayInterface $display): array {
    $groups = $this->getAllNavItemGroups($display);
    if (empty($groups)) {
      return [];
    }

    $items = [];
    foreach ($groups as $group_name => $group) {
      if (!$this->displayItem($group, $build)) {
        continue;
      }

      $attributes = new Attribute();
      if (isset($group['format_settings']['nav_classes'])) {
        $attributes->addClass(explode(' ', $group['format_settings']['nav_classes']));
      }

      $items[$group_name] = [
        'nav_id' => $this->getId($group_name),
        'title' => $group['label'],
        'attributes' => $attributes,
        'weight' => $group['weight'],
      ];
    }

    uasort($items, [SortArray::class, 'sortByWeightElement']);

    return [
      '#theme' => 'page_navigation',
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getId(string $group_name): string {
    return Html::getId('navigation_' . $group_name);
  }

  /**
   * Get all nav item groups.
   *
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface $display
   *   Entity view display.
   *
   * @return array
   *   Nav item groups.
   */
  protected function getAllNavItemGroups(EntityDisplayInterface $display): array {
    return array_filter($display->getThirdPartySettings('field_group'), function ($item) {
      return !empty($item['children']) && $item['format_type'] === 'nav_item';
    });
  }

  /**
   * Check if we want to display item.
   *
   * @param array $group
   *   Group settings.
   * @param array $build
   *   Renderable array.
   *
   * @return bool
   *   True if we want to display an item.
   */
  protected function displayItem(array $group, array $build): bool {
    if ($group['format_settings']['show_empty_fields'] === 1) {
      return TRUE;
    }

    foreach ($group['children'] as $child) {
      if (!empty($build[$child]['#items']) || isset($build[$child]['#theme']) || isset($build[$child]['#type'])) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
