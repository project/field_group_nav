<?php

declare(strict_types=1);

namespace Drupal\field_group_nav;

use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provide interface for navigation service.
 */
interface NavigationBuilderInterface {

  /**
   * Build navigation.
   *
   * @param array $build
   *   A renderable array representing the entity content.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface $display
   *   Entity display.
   *
   * @return array
   *   A render array.
   */
  public function generate(array $build, EntityInterface $entity, EntityDisplayInterface $display): array;

  /**
   * Get Id.
   *
   * @param string $group_name
   *   Group name.
   *
   * @return string
   *   Element Id.
   */
  public function getId(string $group_name): string;

}
