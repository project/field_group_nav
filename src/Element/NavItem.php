<?php

declare(strict_types=1);

namespace Drupal\field_group_nav\Element;

use Drupal\Core\Render\Element\RenderElementBase;

/**
 * Provides a render element for an nav item.
 *
 * @FormElement("field_group_nav_item")
 */
class NavItem extends RenderElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = self::class;

    return [
      '#process' => [
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#theme_wrappers' => ['field_group_nav_item'],
    ];
  }

}
