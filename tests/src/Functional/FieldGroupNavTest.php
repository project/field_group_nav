<?php

declare(strict_types=1);

namespace Drupal\Tests\field_group_nav\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the functionality of the Field Group Navigation module.
 *
 * @group field_group_nav
 */
class FieldGroupNavTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'field',
    'field_ui',
    'field_group',
    'field_group_nav',
    'field_group_nav_test',
  ];

  /**
   * Tests field group navigation behavior.
   */
  public function testFormDisplay() {
    $node = $this->createNode(['type' => 'test_content_nav', 'title' => 'Test Node', 'published' => TRUE]);

    $adminUser = $this->drupalCreateUser([
      'edit any test_content_nav content',
    ]);
    $this->drupalLogin($adminUser);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->assertSession()->elementExists('css', '.page_navigation');
    $this->clickLink('nav plain form group');
    $this->assertSession()->elementExists('css', '#navigation-group-nav-1');
    $this->clickLink('nav number form group');
    $this->assertSession()->elementExists('css', '#navigation-group-nav-2');

    $viewUser = $this->drupalCreateUser([
      'access content',
    ]);
    $this->drupalLogin($viewUser);
    $this->drupalGet($node->toUrl());
    $this->assertSession()->elementExists('css', '.page_navigation');
    $this->clickLink('nav plain group');
    $this->assertSession()->elementExists('css', '#navigation-group-nav-1');
    $this->clickLink('nav number group');
    $this->assertSession()->elementExists('css', '#navigation-group-nav-2');
  }

}
